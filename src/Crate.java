
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class Crate extends GameObject{
    int refill = 15;
    private Handler handler;
    Game game;
    private BufferedImage crate_image;

    public Crate(int x, int y, ID id, SpriteSheet ss) {
        super(x, y, id, ss);
        crate_image = ss.grabImage(6, 2, 32, 32);
    }

    @Override
    public void tick() {
//        for (int i = 0; i <handler.object.size() ; i++) {
//            GameObject tempObject = handler.getObject().get(i);
//            
//            if(tempObject.getId() == ID.Block){
//                if(getBounds().intersects(tempObject.getBounds())){
//                    game.ammo += refill;
//                }
//            }
//            
//        }
    }

    @Override
    public void render(Graphics g) {
        g.drawImage(crate_image, x, y, null);
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle(x, y, 32, 32);
    }
    
}